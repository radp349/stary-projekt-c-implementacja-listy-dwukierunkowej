## README

Ten kod to program w języku C++, który implementuje kolejkę dwukierunkową. Program umożliwia dodawanie i usuwanie elementów na początku i końcu kolejki, dodawanie elementów za wskazanym elementem, usuwanie konkretnego elementu, wyszukiwanie elementu o zadanej wartości atrybutu oraz sortowanie kolejki metodą bąbelkową ze zmianą kierunku przeglądania. Program korzysta z struktur danych do reprezentowania kolejki.

### Kompilacja i Uruchomienie
Aby skompilować ten kod, potrzebujesz kompilatora C++ (np. GNU GCC Compiler) oraz środowiska programistycznego (IDE) takiego jak CodeBlocks 17.12. Postępuj zgodnie z poniższymi krokami, aby skompilować i uruchomić program:

1. Skonfiguruj środowisko programistyczne z CodeBlocks i GNU GCC Compiler.
2. Utwórz nowy projekt C++ w CodeBlocks.
3. Skopiuj kod do pliku źródłowego projektu (np. `main.cpp`).
4. Zapisz kod i zbuduj projekt.
5. Uruchom program, a kolejka będzie operować na przykładowych danych.

### Funkcjonalność
Program udostępnia następującą funkcjonalność w kontekście kolejki dwukierunkowej:

1. **Dodawanie elementu na początku kolejki**: Program pozwala na dodanie nowego elementu na początku kolejki.
2. **Dodawanie elementu na końcu kolejki**: Program pozwala na dodanie nowego elementu na końcu kolejki.
3. **Usuwanie elementu z początku kolejki**: Program usuwa element, który znajduje się na początku kolejki.
4. **Usuwanie elementu z końca kolejki**: Program usuwa element, który znajduje się na końcu kolejki.
5. **Dodawanie elementu za wskazanym elementem**: Program pozwala na dodanie nowego elementu za wskazanym elementem w kolejce.
6. **Usuwanie konkretnego elementu**: Program usuwa element o zadanej wartości atrybutu z kolejki.
7. **Wyszukiwanie elementu o zadanej wartości atrybutu**: Program wyszukuje element o zadanej wartości atrybutu w kolejce.
8. **Wypisywanie elementów kolejki**: Program wypisuje zawartość kolejki na ekranie.
9. **Zwolnienie pamięci zajmowanej przez kolejkę**: Program usuwa wszystkie elementy z kolejki i zwalnia pamięć zajmowaną przez kolejkę.
10. **Sortowanie kolejki metodą bąbelkową, ze zmianą kierunku przeglądania**: Program sortuje kolejkę z użyciem algorytmu sortowania bąbelkowego.

### Struktura danych
Kolejka dwukierunkowa składa się z elementów (`struct element`), które zawierają wartość atrybutu (`zawartosc`), wskaźniki na kolejny (`kolejny`) i poprzedni (`poprzedni`) element w kolejce. Kolejka (`struct kolejka`) przechowuje informację o ilości elementów (`ile`) oraz wskaźniki na początek (`poczatek`) i koniec (`koniec`) kolejki.

### Wejście i Wyjście
Program interaktywnie komunikuje się z użytkownikiem poprzez standardowe wejście i wyjście. Po uruchomieniu programu, użytkownik jest proszony o wybór jednej z opcji od 1 do 10, które odpowiadają różnym operacjom na kolejce. Po wybraniu opcji, program może prosić użytkownika o podanie dodatkowych informacji, takich jak wartość atrybutu elementu do dodania, usunięcia lub wyszukania.

### Ograniczenia
- W tym kodzie wykorzystano prostą implementację kolejki dwukierunkowej przy użyciu struktur danych. Nie uwzględniono innych operacji związanych z kolejką, takich jak kopiowanie, sprawdzanie pustej kolejki itp.
- W przypadku operacji wyszukiwania elementu o zadanej wartości atrybutu, program znajduje tylko pierwsze wystąpienie elementu i zwraca indeks tego elementu w kolejce.
- Program zakłada, że użytkownik podaje poprawne wartości atrybutów podczas dodawania, usuwania i wyszukiwania elementów.

Zaleca się dostarczenie odpowiednich wartości atrybutów podczas interakcji z programem w celu uzyskania poprawnych wyników.
