#include <iostream>
#include <cstdlib>
using namespace std;

void wypisz_menu();
void co_robimy(int &wybor);
//Pojedynczy element kolejki dwukierunkowej
struct element{
    int zawartosc;
    element *kolejny = NULL;
    element *poprzedni = NULL;
};
struct kolejka{
    int ile = 0;
    element *poczatek = NULL;
    element *koniec = NULL;
};

void kolejny(kolejka& lista, int a); //Funkcja dodaje nam do listy kolejny element
int Menager(kolejka& lista);// Funkcja zarzadzajaca programem
void wypisz(kolejka& lista);
void usun_left(kolejka& lista);
void usun_right(kolejka& lista);
void usun(kolejka& lista);
void sortuj(kolejka & lista);
int znajdz(kolejka lista);

int main()
{
    kolejka lista;

    kolejny(lista, 12);
    kolejny(lista, 17);
    kolejny(lista, 14);
    kolejny(lista, 15);

    Menager(lista);

    return 0;
}

void wypisz_menu ()
{
    cout<<"Co chcesz zrobic ? <Podaj liczbe od 1 do 10> "<<endl;
    cout<<"1. Dodac nowy element na poczatku listy"<<endl;
    cout<<"2. Dodac nowy element na koncu listy"<<endl;
    cout<<"3. Usunac element, ktory jest na poczatku listy"<<endl;
    cout<<"4. Usunac element, ktory jest na koncu listy"<<endl;
    cout<<"5. Dodac nowy element za elementem wskazanym"<<endl;
    cout<<"6. Usunac konkretny element"<<endl;
    cout<<"7. Wyszukac element o zadanej wartosci atrybut"<<endl;
    cout<<"8. Wypisac elemnty listy"<<endl;
    cout<<"9. Zwolnic pamiec zajmowana przez liste (koniec programu)"<<endl;
    cout<<"10. Posortowac liste metoda babelkowa, ze zmiana kierunku przegladania"<<endl;
}

void co_robimy(int &wybor)
{
    string Swybor,liczby[10]={"1","2","3","4","5","6","7","8","9","10"};
    bool czy_poprawna_wartosc=0;

    wypisz_menu();

    while(czy_poprawna_wartosc==0)
    {
        cin>>Swybor;
        for(int i=0;i<10;i++)
        {
            if(Swybor==liczby[i])
                {czy_poprawna_wartosc++;}
        }

            if(czy_poprawna_wartosc==0){
                system( "cls" );
                cout<<"Niepoprawne dane! Wpisz liczbe jeszcze raz!"<<endl<<endl;
                wypisz_menu();
            }
    } wybor=atoi(Swybor.c_str());
}

void kolejny(kolejka& lista, int a) //Funkcja dodaje nam do listy kolejny element
{
    lista.ile ++;
    element* add;
    add = new element;
    add->zawartosc = a;

    if(lista.poczatek == NULL) //jesli lista jest pusta kolejny element bedzie pierwszym elementem
    {
        lista.poczatek = add;
        lista.koniec = add;
    }
    else
    {
        //kolejny element dodajemy na koniec listy
        lista.koniec->kolejny = add;
        add->poprzedni = lista.koniec;
        lista.koniec = add;
    }
}
void kolejny_left(kolejka& lista, int a) //Funkcja dodaje nam do listy kolejny element
{
    lista.ile ++;
    element* add;
    add = new element;
    add->zawartosc = a;

    if(lista.poczatek == NULL) //jesli lista jest pusta kolejny element bedzie pierwszym elementem
    {
        lista.poczatek = add;
        lista.koniec = add;
    }
    else
    {
        add->kolejny = lista.poczatek;
        lista.poczatek->poprzedni = add;
        lista.poczatek = add;
    }
}

int usun_o_wartosci(kolejka lista)
{
    int jaki;
    cout<<"jaki element chcesz usunac";
    cin>>jaki;

    element *b = lista.poczatek;
    while(b != NULL)
    {
       if(b->zawartosc == jaki)
       {
           cout<<"znaleziono pod indeksem"<<endl;
            if(b->kolejny != NULL)
            {
                b->kolejny->poprzedni = b->poprzedni;
                b->poprzedni->kolejny = b->kolejny;
               // delete b;
            }
            else
                usun_right(lista);

            return 0;
       }
    b = b->kolejny;
    }
    cout<<"nie znaleziono elementu o zadawej zawartosci"<<endl;
    return 0;
}
int dodac_o_wartosci_za(kolejka lista)
{
    int jaki;
    cout<<"jaki element chcesz dodac"<<endl;
    cin>>jaki;
     int ktory;
    cout<<"za jakim elementem chcesz dodac";
    cin>>ktory;

    element *b = lista.poczatek;
    while(b != NULL)
    {
       if(b->zawartosc == ktory)
       {
            lista.ile ++;
            element* add;
            add = new element;
            add->zawartosc = jaki;
            if(b != NULL)
            {
                add->poprzedni = b;
                add->kolejny = b->kolejny;
                b->kolejny->poprzedni = add;
                b->kolejny = add;
            }
            else
                kolejny(lista, jaki);
            return 0;
       }
    b = b->kolejny;
    }
    cout<<"nie znaleziono elementu o zadawej zawartosci"<<endl;
    return 0;
}

int Menager(kolejka& lista)// Funkcja zarzadzajaca programem
{
    int koniec=0,zmienna_switch, nowy;
    while(koniec==0)
    {
        co_robimy(zmienna_switch);
    switch(zmienna_switch)
    {
        case 1: //1. Dodac nowy element na poczatku listy
            cout<<"Jaki element chcesz dodac na poczatku listy?";
            cin>>nowy;
            kolejny_left(lista, nowy);
            break;
        case 2: //2. Dodac nowy element na koncu listy
            cout<<"Jaki element chcesz dodac na koncu listy?";
            cin>>nowy;
            kolejny(lista, nowy);
            break;
        case 3: //3. Usunac element, ktory jest na poczatku listy
            usun_left(lista);
            break;
        case 4: //4. Usunac element, ktory jest na koncu listy
            usun_right(lista);
            break;
        case 5: //5. Dodac nowy element za elementem wskazanym
            dodac_o_wartosci_za(lista);
            break;
        case 6: //6. Usunac konkretny element
            usun_o_wartosci(lista);
            break;
        case 7: //7. Wyszukac element o zadanej wartosci atrybut
            znajdz(lista);
            break;
        case 8:   //8. Wypisac elemnty listy
            system("cls");
            wypisz(lista);
            break;
        case 9: //9. Zwolnic pamiec zajmowana przez liste (koniec programu)
            usun(lista);
            koniec++;
            break;
        case 10: //10. Posortowac liste metoda babelkowa, ze zmiana kierunku przegladania
            sortuj(lista);
            break;
    }
    }
   return 0;
}

void wypisz(kolejka& lista)
{
    element *b = lista.poczatek;
    while(b != NULL)
    {
        cout<<b->zawartosc<<endl;
        b = b->kolejny;
    }
    cout<<endl;
}

void usun_left(kolejka& lista)
{
    element *a = lista.poczatek;
    lista.poczatek = lista.poczatek->kolejny;
    delete a;
}
void usun_right(kolejka& lista)
{
    element *a = lista.koniec;
    lista.koniec->poprzedni->kolejny = NULL;
    lista.koniec = lista.koniec->poprzedni;
    delete a;
}

void usun(kolejka& lista)
{
    element *c;
    element *b = lista.poczatek;
    while(b != NULL)
    {
        c = b;
        b = b->kolejny;
        delete c;
    }
    lista.poczatek = NULL;
    lista.koniec = NULL;
}

void sortuj(kolejka & lista)
{
    element *a = lista.poczatek;

    for(int i=1; i<lista.ile; i++)
    {
        a = lista.poczatek;
        for(int j=1; j<lista.ile; j++)
        {
            if(a->zawartosc < a->kolejny->zawartosc)
            {
                int c = a->zawartosc;
                a->zawartosc = a->kolejny->zawartosc;
                a->kolejny->zawartosc = c;
            }
            a = a->kolejny;
        }
    }
}

int znajdz(kolejka lista)
{
    int jaki;
    cout<<"jakiego elementu szukasz?";
    cin>>jaki;
    int i = 0;

    element *b = lista.poczatek;
    while(b != NULL)
    {
       if(b->zawartosc == jaki)
       {
           cout<<"znaleziono pod indeksem: "<<i<<endl;
            return 0;
       }
    b = b->kolejny;
    i++;
    }
    cout<<"nie znaleziono elementu o zadawej zawartosci"<<endl;
    return 0;
}
